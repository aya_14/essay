### iOS
##### .gitignore
---

### Taro

##### .gitignore
```
dist/
.temp/
.rn_temp/
node_modules/
.DS_Store
.cache
lerna-debug.log
yarn-error.log
_book
.idea
.vscode
website/build
website/package.json
packages/**/package-lock.json
```
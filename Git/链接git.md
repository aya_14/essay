## gitlab
### macOS
```
// 查看目录下是否有ssh key
$ ~/.ssh
$ ls
```

#### 生成ssh key

**ED25519 SSH key pair:**
```
ssh-keygen -t ed25519 -C "email@example.com"
```
会生成以下文件
```
// 私钥sss
id_ed25519
// 公钥
id_ed25519.pub
```
将公钥内的代码添加到`gitlab > 个人头像 -> setting -> SSH Keys`内<br>

##### Key 添加到ssh-agent
ssh-agent 貌似是对解密的专用密钥进行高速缓存。在windows 没有这一步，linux还需要手动添加到ssh-agent。
```
// 确定ssh-agent是否启用，如果看到Agent pid xxxxx 那就说明已经启用
$ ssh-agent -s

// 把私钥添加到ssh-agent就可以了
$ ssh-add ~/.ssh/id_ed25519
```

或者<br>
**RSA:**
```
ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```
会生成以下文件
```
// 私钥
id_rsa
// 公钥
id_rsa.pub
```
将公钥内的代码添加到`gitlab > 个人头像 -> setting -> SSH Keys`内<br>

##### Key 添加到ssh-agent
ssh-agent 貌似是对解密的专用密钥进行高速缓存。在windows 没有这一步，linux还需要手动添加到ssh-agent。
```
// 确定ssh-agent是否启用，如果看到Agent pid xxxxx 那就说明已经启用
$ ssh-agent -s

// 把私钥添加到ssh-agent就可以了
$ ssh-add ~/.ssh/id_rsa
```

## github
待。。。




















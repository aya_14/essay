### clone
将项目从gitlab上clone下来
```
// SSH or HTTPS
$ git clone <git address>
```

### add & commit
将修改的代码添加到暂存区，准备提交
```
// 将当前文件夹下所有修改的文件添加到暂存区，在.gitignore内指定的文件则不会
$ git add .
$ git commit -m "<what change>"
// <branch>当前分支
$ git push -u origin <branch-name>
```

### pull
```
// 从远程获取最新版本并merge到本地（不推荐这样做）
$ git pull origin <branch-name>

// 从远程获取最新到本地，不会自动merge
$ git fetch origin <branch-name>
// 比较本地<branch-name>与远程<branch-name>的差别
$ git log -p <branch-name> ..origin/<branch-name>
// 合并远程分支（拉取到本地）
$ git merge origin/<branch-name>
// 合并完成之后还得提交一次（很重要）
$ git push -u origin <branch-name>
```

### 分支
```
// 查看分支
$ git branch

// 创建分支
$ git branch <branch-name>

// 切换分支
$ git checkout <branch-name>

// 切换并创建分支
$ git checkout -b <branch-name>

// 命令用于合并指定分支到当前分支
$ git merge -m "<what change>" <branch-name>

// 删除分支
$ git branch -d <branch-name>
// 强制删除
$ git branch -D <branch-name>
```
`master`分支应该是非常稳定的，也就是仅用来发布新版本<br>
`version` 合并的分支<br>
`bug` bug提交、修改的分支<br>
`developer1`开发者1号<br>
`developer2`开发者2号<br>
...<br>
每个人开发都是在自己的分支上，当功能编写完毕后，拉取`version`的代码到自己的分支，合并代码，解决冲突后，再提交到`version`。当发布新版本时，再合并到`master`上。







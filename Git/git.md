# Git
> 分布式版本控制系统

工作区（working directory）
> git所在的工作目录

版本库（repository）
> `.git`文件夹，存放所有版本

版本库分暂存区和分支, `git add`放在暂存区, `git commit`后会提交到分支

---
#### 用户
1. 查看当前用户
> git config user.name

2. 查看当前用户邮箱
> git config user.email

3. 修改用户名和邮箱：
> git config --global user.name "Your_username"
> git config --global user.email "Your_email"
或者改`/Users/liuyuhao/.gitconfig`文件中`[user]`下的username、email

#### 操作
1. 目录git化
> git init

2. 将文件添加到暂存区
> git add `<file>`

3. 将文件提交到分支
> git commit -m "`<message>`"

4. 查看仓库的状态（如：文件修改情况）
> git status

5. 查看文件修改情况
> git diff `<file>`

6. 查看日志
> git log --graph
	- 简化log信息
	> git log `--`pretty=oneline

7. 文件回溯
> git reset --hard HEAD^ (回溯很之前的commit可以用HEAD^n)
	- 或者根据commit id号来指定返回
	> git reset --hard `<commit id>`
	- 撤销已经提交到暂存区的改动
	> git reset HEAD `<file>`	[ HEAD表示当前最新分支 ]

8. 查看所有的提交日志
> git reflog

---
#### 文件
```
目前整个流程就是：修改 -> git add -> 第二次修改 -> git add -> ... -> git commit
```

1. 查看文件内容
> cat `<file>`

2. 丢掉工作区的修改
> git checkout -- `<file>`
```
这时就会有两种状况：
1. 修改后，还没被放到暂存区，现在撤回的话，就回到和版本库一模一样的状态
2. 已经放到暂存区，之后又修改，现在撤回的话，就回到放到暂存区后的状态
```

3. 删除文件
> rm `<file>`

---
#### 远程仓库
```
本地git仓库和GitHub仓库之间的传输通过SSH加密，所以需要将本地生成的id_rsa.pub文件中的密钥与GitHub账号绑定。
```

1. 添加到远程暂存区
> git remote add origin git@gitlab.com:`<username>`/`<repository name>`
```
origin : 默认的远程库名字
```

2. 推送到远程库
> git push -u origin master
```
-u : 1. 本地master分支的内容推送到远程master分支上。
	 2. 把本地master分支和远程master分支关联起来。
```
之后push时，`-u`命令就可以不用了

3. 从远程库克隆
> git clone git@gitlab.com:`<username>`/`<repository name>`

---
#### 分支



























JavaScript 继承机制的设计思想就是，原型对象的所有属性和方法，都能被实例对象共享。也就是说，如果属性和方法定义在原型上，那么所有实例对象就能共享，不仅节省了内存，还体现了实例对象之间的联系。

#### 构造函数的继承
```
function Animal() { }
Animal.prototype.eat = function() {
  console.log('eat something');
}

function Cat() {
  Animal.call(this);
}
Cat.prototype = Object.create(Animal.prototype);
Cat.prototype.constructor = Cat;

var neko = new Cat();
neko.eat();
```
有时只需要单个方法的继承，这时可以采用下面的写法。
```
ClassB.prototype.print = function() {
  ClassA.prototype.print.call(this);
  // some code
}
```
#### 多重继承
```
function M1() {
  this.hello = 'hello';
}

function M2() {
  this.world = 'world';
}

function S() {
  M1.call(this);
  M2.call(this);
}

// 继承 M1
S.prototype = Object.create(M1.prototype);
// 继承链上加入 M2
Object.assign(S.prototype, M2.prototype);

// 指定构造函数
S.prototype.constructor = S;

var s = new S();
s.hello // 'hello'
s.world // 'world'
```

category
```
String.prototype.double = function () {
  return this.valueOf() + this.valueOf();
};

'abc'.double()
// abcabc
```

也可以为某个类型添加公有的变量
```
var s = 'Hello World';
s.x = 123;
s.x // undefined
```
这样就可以了
```
var s = 'Hello World';
String.prototype.x = 123;
s.x // Hello World
```

### 原型链
所有对象都继承了`Object.prototype`的属性，`Object.prototype`的原型是`null`

读取对象的某个属性时，`JavaScript`引擎先寻找对象本身的属性，如果找不到，就到它的原型去找，如果还是找不到，就到原型的原型去找。如果直到最顶层的`Object.prototype`还是找不到，则返回`undefined`。

如果对象自身和它的原型，都定义了一个同名属性，那么优先读取对象自身的属性，这叫做“覆盖”（overriding）

所以，修改原型对象时，一般要同时修改`constructor`属性的指向。

### instanceof
`instanceof`运算符返回一个布尔值，表示对象是否为某个构造函数的实例。

`instanceof`的原理是检查右边构造函数的`prototype`属性，是否在左边对象的原型链上。有一种特殊情况，就是左边对象的原型链上，只有`null`对象。这时，`instanceof`判断会失真。












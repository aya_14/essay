1. 不区分整数和浮点型，统一用`Number`
```javascript
NaN 	 	//not a number
Infinity 	//无限大
null 	 	//空
''   	 	//长度为0的字符串
undefine	//未定义
```

2. `==`与`===`<br>
`==` 自动转换数据类型，会得到很诡异的结果<br>
`===` 不自动转换数据类型，数据类型不一致，则会报`false`
```javascript
NaN === NaN //false
isNaN(NaN)  //true
```

3. `Array`可以通过索引把对应的元素修改为新的值<br>
如果索引超过了范围，Array的大小相应也会变化，没有赋值的会用`undefine`占用

对象转数组
```
Array.prototype.slice.call({ 0: 'a', 1: 'b', length: 2 });
```

4. `iterable`<br>
`Array`,`Map`,`Set`都属于`iterable`<br>
在不确定调用函数有几个参数时，可以使用
```javascript
console.log(arguments);
```
来输出所有参数<br>
或者直接去看原函数<br>

5. 抛出异常
```javascript
throw ''
```

6. `arguments`获取调用者传入的所有参数<br>
`rest`可以获取传入函数的额外参数(...rest)，以数组形式存储，

7. 有个全局变量`window`，全局作用域的变量实际上被绑定到`window`的一个属性，可以通过`window.xx`调用

8. 高阶函数：一个函数接收另一个函数作为参数<br>
map<br>
reduce<br>
filter<br>
sort<br>

9. 闭包：把函数作为结果返回。<br>
函数返回函数，拿一个对象接收返回值，那个对象也是函数（对象指针指向了函数返回的函数，这个对象便成了返回的函数）<br>
闭包中`this`指向当前函数的上一级函数

10. 箭头函数：用箭头定义函数
```javascript
x => x * x;

等价于

function(x) {
	return x * x;
}
```
多条语句得用`{}`<br>
箭头函数中`this`总是指向词法作用域，也就是最外层调用者。<br>
箭头函数不会创建自己的`this`,它只会从自己的作用域链的上一层继承`this`。

11. `yield`
ES6 新关键字，使生成器函数执行暂停，`yield`关键字后面的表达式的值返回给生成器的调用者，可以被认为是一个基于生成器版本的`return`关键字。<br>
返回`IteratorResult`对象，{value: , done: }

12. 对象
`JavaScript`不区分类和实例的概念，而是通过原型来实现面向对象编程<br>
所有对象都是实例，所谓继承关系不过是把一个对象的原型指向另一个对象而已
```javascript
obj.__proto__

Object.create(<class>) //基于<class>原型创建一个新的对象
```
待。。。

13. 原型链
`Array`对象
```javascript
arr -> Array.prototype -> Object.prototype -> null
```
函数
```
foo1 -v
foo ---> Function.prototype -> Object.prototype -> null
foo2 -^
...
```

14. 构造函数
用`new`调用这个函数，并返回一个对象，它绑定的`this`指向新创建的对象，并默认返回`this`
```javascript
var lyh = new Student('lyh');
```
以上这种方式创建的对象还从原型上获得了`constructor`属性，它指向`Student`函数本身
```javascript
lyh.constructor === Student.prototype.constructor // true
Student.prototype.constructor === Student
```

15. export & import
`export`用于对外输出本模块（一个文件可以理解为一个模块）变量的接口<br>
`import`用于在一个模块中加载另一个含有`export`接口的模块。<br>

export跟export default 有什么区别呢？如下：

1、export与export default均可用于导出常量、函数、文件、模块等<br>
2、你可以在其它文件或模块中通过import+(常量 | 函数 | 文件 | 模块)名的方式，将其导入，以便能够对其进行使用<br>
3、在一个文件或模块中，export、import可以有多个，export default仅有一个<br>
4、通过export方式导出，在导入时要加{ }，export default则不需要<br>

16. 避免了污染全局变量
```
(function () {
  var tmp = newData;
  processData(tmp);
  storeData(tmp);
}());
```





























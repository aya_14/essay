#### this
- 不管是不是在函数内部，只要是在全局环境下运行，`this`就是指顶层对象`window`。

- 构造函数中的`this`，指的是实例对象。
```
var Obj = function (p) {
  this.p = p;
};

var o = new Obj('Hello World!');
o.p // "Hello World!"
```
由于`this`指向实例对象，所以在构造函数内部定义`this.p`，就相当于定义实例对象有一个`p`属性。

- 如果对象的方法里面包含`this`，`this`的指向就是方法运行时所在的对象。
```
var obj ={
  foo: function () {
    console.log(this);
  }
};

obj.foo() // obj
```
该方法赋值给另一个对象，就会改变`this`的指向。<br>

内层的`this`不指向外部，而指向顶层对象。
解决办法：

- 使用中间变量固定this。`var that = this;`
- 将`this`当作函数方法的第二个参数，固定它的运行环境。
- 使用箭头函数`() => {}`

### JavaScript 提供了call、apply、bind这三个方法，来切换/固定this的指向。

#### call(thisObj, Object)
> func.call(thisValue, arg1, arg2, ...)

> `thisValue`就是`this`所要指向的那个对象，后面的参数则是函数调用时所需的参数。

定义：调用一个对象的一个方法，以另一个对象替换当前对象。<br>
函数实例的call方法，可以指定函数内部this的指向（即函数执行时所在的作用域），然后在所指定的作用域中，调用该函数。

#### apply(thisObj, [argArray])
> func.apply(thisValue, [arg1, arg2, ...])

定义：应用某一对象的一个方法，用另一个对象替换当前对象。
apply方法的作用与call方法类似，也是改变this指向，然后再调用该函数。唯一的区别就是，它接收一个数组作为函数执行时的参数。

```

```

##### 调用函数，传递参数
```
function add(x, y) {
    return x + y;
}

function myAddCall(x, y) {
    return add.call(this, x, y);
}

function myAddApply(x, y) {
    return add.apply(this, [x, y]);
}

console.log(myAddCall(10, 20));    //输出结果30

console.log(myAddApply(20, 20));  //输出结果40
```

#### bind
`bind`方法用于将函数体内的`this`绑定到某个对象，然后返回一个新函数。
```
var d = new Date();
var print = d.getTime.bind(d);
print() // 1555468984506
```
...














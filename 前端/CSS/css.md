#### 父元素和兄弟元素
- 父元素：包含关系
- 兄弟元素：属于被同一个元素包含的平级元素

#### id 和 class 选择器
`id 选择器`可以为标有`特定id`的`HTML元素`指定特定的样式。<br>
`id 选择器`以`#`来定义<br>

`class 选择器`用于描述一组元素的样式。class可以在`多个元素`中使用<br>
`class 选择器`以`.`来定义<br>

#### 伪类
CSS伪类（pseudo-class）是加在选择器后面的用来指定元素状态的关键字。


#### 插入样式表
1. 外部样式表
```
<head>
	<link rel="stylesheet" type="text/css" href="<css path>">
</head>
```

2. 内部样式表
```
<head>
	<style>
	hr {color:sienna;}
	p {margin-left:20px;}
	body {background-image:url("images/back40.gif");}
	</style>
</head>
```

3. 内联样式
```
<p style="color:sienna;margin-left:20px">这是一个段落。</p>
```

注：如果某些属性在不同的样式表中被同样的选择器定义，那么属性值将从更具体的样式表中被继承过来。<br>
优先级：内联样式）Inline style > （内部样式）Internal style sheet >（外部样式）External style sheet > 浏览器默认样式

#### padding
如果提供全部四个参数值，将按上－右－下－左的顺序作用于四边。（ 顺时针 ）<br>
如果只提供一个，将用于全部的四条边。<br>
如果提供两个，第一个用于上－下，第二个用于左－右。<br>
如果提供三个，第一个用于上，第二个用于左－右，第三个用于下。<br>

#### 嵌套选择器
- `p { }`：为所有`p元素`指定一个样式
- `.marked { }`：为所有`class="marked"`的元素指定一个样式
- `.marked p { }`：为所有`class="marked"`元素内的`p元素`指定一个样式
- `p.marked { }`：为所有`class="marked"`的`p元素`指定一个样式

#### 显示
`display: none`隐藏某个元素，且隐藏的元素不会占用任何空间。<br>
`visibility: hidden`隐藏某个元素，但隐藏的元素会占用与未隐藏之前一样的空间。

#### 块(block)和内联(inline)
`块`元素，占用了全部宽度，在前后都是换行符。<br>
```
address , blockquote , center , dir , div , dl , fieldset , form , h1 , h2 , h3 , h4 , h5 , h6 , hr , isindex , menu , noframes , noscript , ol , p , pre , table , ul , li
```
`内联`元素，只需要必要的宽度，不强制换行。<br>
```
a , abbr , acronym , b , bdo , big , br , cite , code , dfn , em , font , i , img , input , kbd , label , q , s , samp , select , small , span , strike , strong , sub , sup ,textarea , tt , u , var
```
可以更改内联元素和块元素，反正亦然，可以使页面看起来是以一种特定的方式组合。
```
display:block  -- 显示为块级元素
display:inline  -- 显示为内联元素
display:inline-block -- 显示为内联块元素，表现为同行显示并可修改宽高内外边距等属性
```

#### 定位(position)
`static`：HTML元素的默认值，即没有定位，遵循正常的文档流对象
> 不会受top、bottom、left、right的影响

`relative`：相对定位元素的定位是相对其正常位置
> 原来空间仍然会被它占据

`fixed`：元素的位置相对于浏览器窗口是固定位置。即窗口是滚动的它也不会移动

`absolute`：绝对定位的元素的位置相对于最近的已定位父元素，如果没有，则相对于`<html>`
> 会与其他元素重叠

`sticky`：基于用户的滚动位置来定位。元素定位表现在跨越特定阈值前为相对定位，之后为固定定位
> 在safari中需要`-webkit-sticky`

`z-index`属性定义了一个元素的堆叠顺序，具有更高堆叠顺序的元素总是在较低的前面

#### 浮动
`float`：元素会尽量向左或向右移动，直到它的外边缘碰到包含框或另一个浮动框的边框为止，其周围的元素也会重新排列。

`clear`：指定元素两侧不能出现浮动元素。

#### 居中
- 元素居中对齐：`margin: auto;`
> 如果没有设置`width`（或者width: 100%;），居中都不会起效。

- 文本居中对齐：`text-align: center;`

- 图片居中对齐：
```
img {
	display: block;
	marign: auto;
	width: 14%;
}
```

#### 组合选择符
- 后代选择器（以' '分离）：用于选取某元素的后代元素
```
div p {
	/* to do */
}
```
> 只要是在`<div>`内的`<p>`，都受影响。

- 子元素选择器（以'>'分离）：只能选择作为某元素子元素的元素
```
div>p {
	/* to do */
}
```
> 受影响的`<p>`只能在`<div>`下，若`<div>`中用`<span>`包裹了`<p>`，那么此时的`<p>`并不会受影响

- 相邻兄弟选择器（以'+'分离）：选择**紧接**在另一元素后的元素，且二者有相同父元素
```
div+p {
	/* to do */
}
```
> 紧跟`<div>`的`<p>`会受影响

- 普通兄弟选择器（以'~'分离）：选取所有指定元素之后的相邻兄弟元素
```
div~p {
	/* to do */
}
```
> `<div>`后所的`<p>`都会受影响


#### 伪类











#### package.json
管理本地安装的npm包

创建`package.json`
> npm init

#### 常用命令
mac 全局安装路径 `/usr/local/lib/node_modules`

##### 包管理
生产环境`dependencies`
> npm install `<package_name>` --save

开发环境`devDependencies`
> npm install `<package_name>` --save-dev
---

安装本地包（如果你自己的模块依赖于某个包，并通过 Node.js 的 require 加载，那么你应该选择本地安装）
> npm install `<package_name>`

安装全局包（如果你想将包作为一个命令行工具，（比如 grunt CLI），那么你应该选择全局安装）
> npm install -g `<package_name>`
---

列出过时的包（低于需要的版本）
> npm outdated

更新指定本地包（要在项目下执行）
> npm update `<package_name>`
- 更新所有的
> npm update

更新指定全局包
> npm update -g `<package_name>`
- 更新所有的
> npm update -g
---

卸载本地包
> npm uninstall `<package_name>`

- 从`package.json` `dependencies`删除
> npm uninstall --save `<package_name>`
- `devDependencies`
> npm uninstall --save-dev `<package_name>`

卸载全局包
> npm uninstall -g `<package_name>`



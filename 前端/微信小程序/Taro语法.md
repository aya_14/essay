#### 页面流程
```
// 将要挂载
componentWillMount () { }

// 已经挂载
componentDidMount () { }

// 页面卸载
componentWillUnmount () { }

// 从后台进入前台时触发
componentDidShow () { }

// 从前台进入后台时触发
componentDidHide () { }
```
似乎一开始会把所有界面的`render() { }`都会加载，然后再加载当前页面的`render() { }`。

#### 导航栏跳转
若要传参数 `?param1=${param1}&param2=${param2}`
```javascript
Taro.navigateTo({
	url: '<path>?param1=${param1}&param2=${param2}'
});
```

#### 获取路由参数
```
// 所有参数
this.$router.params
// 获取单个参数
this.$router.params['param1']
this.$router.params['param2']
```
### 启动
1. 使用命令创建模板项目
> taro init [App]

2. 启动监听
> npm run dev:weapp

### Taro文件结构与微信小程序
#### app.js（有待确认）
> https://developers.weixin.qq.com/miniprogram/dev/framework/config.html

Taro中对应微信小程序的全局配置，包括了小程序的所有页面路径、界面表现、网络超时时间、底部 tab 等。<br>

#### app.css
app.js对应的css文件

#### project.config.json
> https://developers.weixin.qq.com/miniprogram/dev/devtools/projectconfig.html

（针对于微信开发者工具）你在工具上做的任何配置都会写入到这个文件，当你重新安装工具或者换电脑工作时，你只要载入同一个项目的代码包，开发者工具就自动会帮你恢复到当时你开发项目时的个性化配置，其中会包括编辑器的颜色、代码上传时自动压缩等等一系列选项。

### index.html
小程序的‘底’，app.js的依赖对象。

### .editorconfig
> https://editorconfig.org/

### .eslintrc

### .gitignore
git commit时的文件忽略列表

### package.json
> http://javascript.ruanyifeng.com/nodejs/packagejson.html

定义了这个项目所需要的各种模块，以及项目的配置信息（比如名称、版本、许可证等元数据）。








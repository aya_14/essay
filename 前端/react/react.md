React 是一个声明式，高效且灵活的用于构建用户界面的 JavaScript 库。

---

#### 组件
使用 React 可以将一些简短、独立的代码片段组合成复杂的 UI 界面，这些代码片段被称作“组件”。


#### props && state
在 React 中，有一个命名规范，通常会将代表事件的的监听`prop`命名为`on[Event]`，将处理事件的监听方法命名为`handle[Event]`这样的格式。

当你遇到需要同时获取多个子组件数据，或者两个组件之间需要相互通讯的情况时，需要把子组件的 state 数据提升至其共同的父组件当中保存。之后父组件可以通过`props`将状态数据传递到子组件当中。 这样应用当中所有组件的状态数据就能够更方便地同步共享了。

出于性能考虑，React 可能会把多个 setState() 调用合并成一个调用。
因为 this.props 和 this.state 可能会异步更新，所以你不要依赖他们的值来更新下一个状态。
要解决这个问题，可以让 setState() 接收一个函数而不是一个对象。

class 的方法默认不会绑定 this。如果你忘记绑定 this.handleClick 并把它传入了 onClick，当你调用这个函数的时候 this 的值为 undefined。


#### keys
Keys 帮助 React 识别哪些元素改变了，比如被添加或删除。<br>
一个元素的 key 最好是这个元素在列表中拥有的一个独一无二的字符串。<br>
当元素没有确定 id 的时候，万不得已你可以使用元素索引 index 作为 key
```javascript
const todoItems = todos.map((todo, index) =>
  // Only do this if items have no stable IDs
  <li key={index}>
    {todo.text}
  </li>
);
```
如果你选择不指定显式的键值，那么 React 将默认使用索引用作为列表项目的键值。<br>
数组元素中使用的 key 在其兄弟节点之间应该是独一无二的。然而，它们不需要是全局唯一的。


#### React DOM
React DOM 会将元素和它的子元素与它们之前的状态进行比较，并只会进行必要的更新来使 DOM 达到预期的状态。


#### 生命周期
1. 组件已经被渲染到 DOM 中后
```javascript
componentDidMount()
```
2. 组件将要被卸载
```javascript
componentWillUnmount()
```


#### 传值
react添加属性必须是以`data-xx`形式
```
<Text data-parentindex={index}></Text>
```

currentTarget  事件属性返回其监听器触发事件的节点，即当前处理该事件的元素、文档或窗口。
target		   事件属性返回事件的目标节点（触发该事件的节点），如生成事件的元素、文档或窗口。


#### ( )
```
const About = () => (
  <div>
    <h2>About</h2>
  </div>
)
```
相当于
```
const About = () => {
    return (
      <div>
        <h2>About</h2>
      </div>
    )
}
```
同样适用于js中的map等。。。


#### 多个className
```
<Image src={arrowDownPng} className={`icon_down ${this.state.listShow[index]?"icon_down_rotate":null}`}></Image>
```


#### defaultProps
```
static defaultProps = {
        list: [],
    }
```
为组件提供默认参数


#### Refs(勿过度使用)
> Refs 提供了一种方式，允许我们访问 DOM 节点或在 render 方法中创建的 React 元素。

> 不管在任何情况下，Taro 都推荐你使用函数的方式创建 ref。

使用 refs 在你的 app 中“让事情发生”。
下面是几个适合使用 refs 的情况：

- 管理焦点，文本选择或媒体播放。
- 触发强制动画。
- 集成第三方 DOM 库。
避免使用 refs 来做任何可以通过声明式实现来完成的事情。

当 ref 被传递给 render 中的元素时，对该节点的引用可以在 ref 的 current 属性中被访问。

React 将在组件挂载时，会调用 ref 回调函数并传入 DOM 元素，当卸载时调用它并传入 null。在 componentDidMount 或 componentDidUpdate 触发前，React 会保证 refs 一定是最新的。



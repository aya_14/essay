Toggle 组件会渲染一个让用户切换开关状态的按钮
```javascript
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // 为了在回调中使用 `this`，这个绑定是必不可少的
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    return (
      // 或者把绑定写在这
      // <button onClick={this.handleClick.bind(this)}>
      // 那么`this.handleClick = this.handleClick.bind(this);`这句就没必要了
      // 若要传参数，那么参数依次在前面，最后的参数则是this对象
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);
```
你必须谨慎对待 JSX 回调函数中的 this，在 JavaScript 中，class 的方法默认不会绑定 this。如果你忘记绑定 this.handleClick 并把它传入了 onClick，当你调用这个函数的时候 this 的值为 undefined。


如果觉得使用 bind 很麻烦，这里有两种方式可以解决:

1. 如果你正在使用实验性的 public class fields 语法，你可以使用 class fileds 正确的绑定回调函数：
```javascript
// 这种语法确保 `this` 绑定在 `handleClick` 内。
// 注意: 这是 *实验性* 语法。
handleClick = () => {
  console.log('this is:', this);
}
```

2. 如果你没有使用 class fileds 语法，你可以在回调中使用箭头函数：
```javascript
render() {
  // 这种语法确保 `this` 绑定在 `handleClick` 内。
  return (
    <button onClick={(e) => this.handleClick(e)}>
      Click me
    </button>
  );
}
```
此语法问题在于每次渲染时都会创建不同的回调函数。在大多数情况下，这没什么问题，但如果该回调函数作为prop传入子组件时，这些组件可能会进行额外的重新渲染。

##### 我们通常建议在构造器中绑定或使用 class fileds 语法来避免这类性能问题。

管理node.js版本
---

### windows:
#### nvm
nvm安装路径: `E:\dev\nvm`<br>
nodejs的目录链接: `E:\dev\nodejs`

查看nvm是否安装成功，若没有，检查下环境变量设置中的`NVM_HOME`,`NVM_SYMLINK`
> nvm

在nvm的目录下，创建两个文件夹<br>
`node_global`
> npm config set prefix: "E:\dev\nvm\node_global"

`node_cache`
> npm config set cache: "E:\dev\nvm\node_cache"

修改setting.txt:
```
root: E:\dev\nvm									/* nvm路径 */
path: E:\dev\nodejs									/* nodejs路径 */
arch: 64											/* 操作系统位数 */
proxy: none											/* 代理配置，不管 */
node_mirror: http://npm.taobao.org/mirrors/node/	/* node下载镜像地址 */
npm_mirror: https://npm.taobao.org/mirrors/npm/		/* npm下载镜像地址 */
```

#### 环境变量（系统）
添加：
```
NVM_HOME
: E:\dev\nvm
```
```
NVM_SYMLINK
: E:\dev\nodejs
```
```
NODE_PATH
: E:\dev\nvm\node_global\node_modules
注：这是npm安装全局模组的位置，设置后创建的项目就可以直接引用了。
注：npm安装模组时，后面加上-g就会下载到这，否则，就是在项目中了。
```
修改：
```
Path
: %NVM_HOME%;%NVM_SYMLINK%;%NODE_PATH%;...
注：一定要放在最前面
```
---

查看本地的nodejs版本
> nvm list

安装
> nvm install [version] / latest（最新版本）

使用指定版本
> nvm use [version]


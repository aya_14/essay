#!/usr/bin/env bash
cookie=$1
url=$2

ffmpeg -y \
  -headers "Cookie: ${cookie}" \
  -user_agent 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36' \
  -i "${url}" -c copy test.m3u8
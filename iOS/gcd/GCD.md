## 结构

```objective-c
dispatch_group_t（组）
  dispatch_queue_t（队列）
  	async（异步行为）
  	sync （同步行为）
```

##### dispatch_group_t

```objective-c
NSLog(@"main: %@",[NSThread mainThread]);

dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
dispatch_group_t group = dispatch_group_create();
dispatch_group_async(group, queue, ^{
	NSLog(@"111： %@", [NSThread currentThread]);
});
dispatch_group_async(group, queue, ^{
	NSLog(@"222： %@", [NSThread currentThread]);
});
dispatch_group_async(group, queue, ^{
	NSLog(@"333： %@", [NSThread currentThread]);
});

dispatch_group_notify(group, queue, ^{
	NSLog(@"done");
});
```

输出结果：

```objective-c
main: <NSThread: 0x600000e10980>{number = 1, name = main}
222： <NSThread: 0x600000e54080>{number = 6, name = (null)}
111： <NSThread: 0x600000e52e00>{number = 5, name = (null)}
333： <NSThread: 0x600000e29c00>{number = 3, name = (null)}
done
```

## 基础

- 串行（DISPATCH_QUEUE_SERIAL）：一个任务执行完毕后，再执行下一个任务。有且只能存在一个子线程

- 并行：多个任务同时执行

- 异步（async）：不阻塞当前线程操作，指线程的异步。通常异步操作都是开启另一个线程来执行，开启的这个只能是子线程

- 同步：一步一步按顺序执行线程内的东西

同步和异步决定了是否开启新线程（或者说是否具有开启新线程的能力）

串行和并发决定了任务的执行方式——串行执行还是并发执行（或者说开启多少条新线程）

#### 异步 + 串行

```objective-c
NSLog(@"main: %@",[NSThread mainThread]);

dispatch_queue_t queue = dispatch_queue_create("serial", DISPATCH_QUEUE_SERIAL);
dispatch_async(queue, ^{
  NSLog(@"111:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"222:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"333:  %@",[NSThread currentThread]);
});

dispatch_queue_t queue2 = dispatch_queue_create("serial2", DISPATCH_QUEUE_SERIAL);
dispatch_async(queue2, ^{
  NSLog(@"111_2:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"222_2:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"333_2:  %@",[NSThread currentThread]);
});
```
输出结果：

```objective-c
main:   <NSThread: 0x600002b04140>{number = 1, name = main}
111_2:  <NSThread: 0x600002b49c00>{number = 6, name = (null)}
111:    <NSThread: 0x600002b543c0>{number = 5, name = (null)}
222_2:  <NSThread: 0x600002b49c00>{number = 6, name = (null)}
222:    <NSThread: 0x600002b543c0>{number = 5, name = (null)}
333_2:  <NSThread: 0x600002b49c00>{number = 6, name = (null)}
333:    <NSThread: 0x600002b543c0>{number = 5, name = (null)}

or

// 很奇怪啊
main:   <NSThread: 0x6000016383c0>{number = 1, name = main}
111:    <NSThread: 0x600001614640>{number = 5, name = (null)}
222:    <NSThread: 0x600001614640>{number = 5, name = (null)}
333:    <NSThread: 0x600001614640>{number = 5, name = (null)}
111_2:  <NSThread: 0x600001614640>{number = 5, name = (null)}
222_2:  <NSThread: 0x600001614640>{number = 5, name = (null)}
333_2:  <NSThread: 0x600001614640>{number = 5, name = (null)}

or

main:   <NSThread: 0x600002a54740>{number = 1, name = main}
111:    <NSThread: 0x600002a638c0>{number = 3, name = (null)}
111_2:  <NSThread: 0x600002a1c400>{number = 5, name = (null)}
222_2:  <NSThread: 0x600002a1c400>{number = 5, name = (null)}
222:    <NSThread: 0x600002a638c0>{number = 3, name = (null)}
333_2:  <NSThread: 0x600002a1c400>{number = 5, name = (null)}
333:    <NSThread: 0x600002a638c0>{number = 3, name = (null)}
```

队列数决定线程数，而且每个队列内只存在一个子线程。

#### 异步 + 并行：

```objective-c
NSLog(@"main: %@",[NSThread mainThread]);

dispatch_queue_t queue = dispatch_queue_create("concurrent", DISPATCH_QUEUE_CONCURRENT);
dispatch_async(queue, ^{
  NSLog(@"111:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"222:  %@",[NSThread currentThread]);
});
dispatch_async(queue, ^{
  NSLog(@"333:  %@",[NSThread currentThread]);
});
```

输出结果：

```objective-c
main: <NSThread: 0x600001920ec0>{number = 1, name = main}
111:  <NSThread: 0x600001970440>{number = 5, name = (null)}
333:  <NSThread: 0x600001926580>{number = 3, name = (null)}
222:  <NSThread: 0x600001965280>{number = 6, name = (null)}
```

有多少异步任务，便开启多少异步线程

#### 同步 + 串行：

```objective-c
NSLog(@"main: %@",[NSThread mainThread]);

dispatch_queue_t queue = dispatch_queue_create("serial", DISPATCH_QUEUE_SERIAL);
dispatch_sync(queue, ^{
	NSLog(@"111:  %@",[NSThread currentThread]);
});
dispatch_sync(queue, ^{
	NSLog(@"222:  %@",[NSThread currentThread]);
});
dispatch_sync(queue, ^{
	NSLog(@"333:  %@",[NSThread currentThread]);
});

dispatch_queue_t queue2 = dispatch_queue_create("serial2", DISPATCH_QUEUE_SERIAL);
dispatch_sync(queue2, ^{
	NSLog(@"111_2:  %@",[NSThread currentThread]);
});
dispatch_sync(queue2, ^{
	NSLog(@"222_2:  %@",[NSThread currentThread]);
});
dispatch_sync(queue2, ^{
	NSLog(@"333_2:  %@",[NSThread currentThread]);
});
```

输出结果：

```objective-c
main:   <NSThread: 0x600001460f00>{number = 1, name = main}
111:    <NSThread: 0x600001460f00>{number = 1, name = main}
222:  	<NSThread: 0x600001460f00>{number = 1, name = main}
333:  	<NSThread: 0x600001460f00>{number = 1, name = main}
111_2:  <NSThread: 0x600001460f00>{number = 1, name = main}
222_2:  <NSThread: 0x600001460f00>{number = 1, name = main}
333_2:  <NSThread: 0x600001460f00>{number = 1, name = main}
```

按顺序执行

#### 同步 + 并行：

```objective-c
NSLog(@"main: %@",[NSThread mainThread]);

dispatch_queue_t queue = dispatch_queue_create("concurrent", DISPATCH_QUEUE_CONCURRENT);
dispatch_async(queue, ^{
	NSLog(@"111:  %@",[NSThread currentThread]);
});
dispatch_sync(queue, ^{
	NSLog(@"222:  %@",[NSThread currentThread]);
});
dispatch_sync(queue, ^{
	NSLog(@"333:  %@",[NSThread currentThread]);
});

dispatch_queue_t queue2 = dispatch_queue_create("concurrent2", DISPATCH_QUEUE_CONCURRENT);
dispatch_sync(queue2, ^{
	NSLog(@"111_2:  %@",[NSThread currentThread]);
});
dispatch_sync(queue2, ^{
	NSLog(@"222_2:  %@",[NSThread currentThread]);
});
dispatch_sync(queue2, ^{
	NSLog(@"333_2:  %@",[NSThread currentThread]);
});
```

输出结果：

```objective-c
main: <NSThread: 0x600002eb8380>{number = 1, name = main}
111:  <NSThread: 0x600002eb8380>{number = 1, name = main}
222:  <NSThread: 0x600002eb8380>{number = 1, name = main}
333:  <NSThread: 0x600002eb8380>{number = 1, name = main}
111_2:  <NSThread: 0x600002eb8380>{number = 1, name = main}
222_2:  <NSThread: 0x600002eb8380>{number = 1, name = main}
333_2:  <NSThread: 0x600002eb8380>{number = 1, name = main}
```

按顺序执行，任你多少队列也不管用

---

## 队列

串行队列：每个队列只会开启一个线程，里面有多少任务都只开启一个
并行队列：

注意：
1. 主线程中不能使用同步。会发生循环等待。主线程和主队列的相互等待。主线程内的东西需要加入到队列执行，而主队列在等待主线程执行完毕再继续执行。因此死循环，且主线程内代码不会执行

2. 关于刷新UI的事情需要回归主线程来做。子线程不具备刷新UI的功能。可以更新的结果只是一个幻像：因为子线程代码执行完毕了，又自动进入到了主线程，执行了子线程中的UI更新的函数栈

#### 队列创建
```objective-c
//串行
dispatch_queue_t queueSerial = dispatch_queue_create("testSerial", DISPATCH_QUEUE_SERIAL);
//并行
dispatch_queue_t queueConcurrent = dispatch_queue_create("testConcurrent", DISPATCH_QUEUE_CONCURRENT);
```
#### 任务创建
```objective-c
//同步
dispatch_sync(queueSerial, ^{
        
});
//异步
dispatch_async(queueSerial, ^{
        
});
```
### 并行 + 同步

> 不会开启新线程，执行完一个任务后，继续执行下一个。

```objective-c
NSLog(@"sync - concurrent begin");
    
dispatch_queue_t queue = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"sync - Concurrent end");
```
#### 结果:
```objective-c
sync - concurrent begin
1------<NSThread: 0x608000065480>{number = 1, name = main}
1------<NSThread: 0x608000065480>{number = 1, name = main}
2------<NSThread: 0x608000065480>{number = 1, name = main}
2------<NSThread: 0x608000065480>{number = 1, name = main}
3------<NSThread: 0x608000065480>{number = 1, name = main}
3------<NSThread: 0x608000065480>{number = 1, name = main}
sync - Concurrent end
```
### 并行 + 异步
> 可同时开启多线程，任务交替执行
```objective-c
NSLog(@"async - Concurrent begin");
    
dispatch_queue_t queue = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"async - Concurrent end");
```
#### 结果:
> 任务执行的顺序并不一定是123123，也有可能是213213...交替着同时执行。

> 所有任务是在end之后执行的，说明将所有任务添加到队列之后，才开始异步执行的。
```objective-c
async - Concurrent begin
async - Concurrent end
1------<NSThread: 0x6000000742c0>{number = 3, name = (null)}
2------<NSThread: 0x60800006f040>{number = 4, name = (null)}
3------<NSThread: 0x61800006b700>{number = 5, name = (null)}
1------<NSThread: 0x6000000742c0>{number = 3, name = (null)}
2------<NSThread: 0x60800006f040>{number = 4, name = (null)}
3------<NSThread: 0x61800006b700>{number = 5, name = (null)}
```

### 串行 + 同步
> 不会开启新线程，在当前线程执行任务。任务是串行的，执行完一个任务，再执行下一个任务
```objective-c
NSLog(@"sync - Serial begin");
dispatch_queue_t queue = dispatch_queue_create("test", DISPATCH_QUEUE_SERIAL);
    
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"sync - Serial end");
```
#### 结果:
```objective-c
sync - Serial begin
1------<NSThread: 0x618000066e00>{number = 1, name = main}
1------<NSThread: 0x618000066e00>{number = 1, name = main}
2------<NSThread: 0x618000066e00>{number = 1, name = main}
2------<NSThread: 0x618000066e00>{number = 1, name = main}
3------<NSThread: 0x618000066e00>{number = 1, name = main}
3------<NSThread: 0x618000066e00>{number = 1, name = main}
sync - Serial end
```

串行 + 异步
> 会开启新线程，但是因为任务是串行的，执行完一个任务，再执行下一个任务
```objective-c
NSLog(@"async - Serial begin");
dispatch_queue_t queue = dispatch_queue_create("test", DISPATCH_QUEUE_SERIAL);
    
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"async - Serial end");
```
#### 结果:
```objective-c
async - Serial begin
async - Serial end
1------<NSThread: 0x610000069600>{number = 3, name = (null)}
1------<NSThread: 0x610000069600>{number = 3, name = (null)}
2------<NSThread: 0x610000069600>{number = 3, name = (null)}
2------<NSThread: 0x610000069600>{number = 3, name = (null)}
3------<NSThread: 0x610000069600>{number = 3, name = (null)}
3------<NSThread: 0x610000069600>{number = 3, name = (null)}
```

### 主队列 + 同步
> 互等卡住不可行(在主线程中调用)
```objective-c
NSLog(@"sync - Main begin");
dispatch_queue_t queue = dispatch_get_main_queue();
    
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_sync(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"sync - Main end");
```
#### 结果:
```
sync - Main begin
```

### 主队列 + 异步
> 只在主线程中执行任务，执行完一个任务，再执行下一个任务
```objective-c
NSLog(@"async - Main begin");
dispatch_queue_t queue = dispatch_get_main_queue();
    
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"2------%@",[NSThread currentThread]);
    }
});
dispatch_async(queue, ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"3------%@",[NSThread currentThread]);
    }
});
    
NSLog(@"async - Main end");
```
#### 结果:
```objective-c
async - Main begin
async - Main end
1------<NSThread: 0x618000262780>{number = 1, name = main}
1------<NSThread: 0x618000262780>{number = 1, name = main}
2------<NSThread: 0x618000262780>{number = 1, name = main}
2------<NSThread: 0x618000262780>{number = 1, name = main}
3------<NSThread: 0x618000262780>{number = 1, name = main}
3------<NSThread: 0x618000262780>{number = 1, name = main}
```

# GCD线程之间的通讯
将耗时的操作放在其他线程中，等执行完成后，再回主线程更新。例如，图片下载，文件上传等。
```objective-c
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    for (int i = 0; i < 2; ++i) {
        NSLog(@"1------%@",[NSThread currentThread]);
    }
        
    // 回到主线程
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"2-------%@",[NSThread currentThread]);
    });
});
```
![1187684-e0f9c4c70ee3c784](img/1187684-e0f9c4c70ee3c784.png)

`iOS 5`之前是MRC，内存需要程序员进行管理（用retainCount来管理对象，为0时释放对象），`iOS 5`之后是ARC，除非特殊情况，比如C框架或者循环引用，其他时候是不需要程序员手动管理内存的。

![3691932-2755b62e1b3700d0](img/3691932-2755b62e1b3700d0.png)

1. `alloc`：为对象分配内存，retainCount 为1 
2. `retain`： MRC下 retainCount + 1
3. `copy`：一个对象变成新的对象，retainCount为 1， 原有的对象计数不变
4. `release`：对象的引用计数 -1
5. `autorelease`：对象的引用计数 retainCount - 1，如果为0，等到最近一个pool结束时释放



## 非@property

```objective-c
@interface ViewController (){
    int _age;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _age = 10;
    self.age = 10;
    NSLog(@"%d", self.age);
  	NSLog(@"%d", _age);
}

- (int)age {
    return _age;
}

- (void)setAge:(int)age {
    _age = age;
}
```

__赋值时__：只有`self.age`会调用`setter`方法

__取值时__：只有`self.age`调用`getter`方法

所以，实质上来说，`self.age`只是使系统调用`_age`的通道

赋值时，`self.age = 10; 等价于 [self setAge:10];`，调用`setter`方法，将`参数age`赋值给`_age`，使得`_age`有值；

取值时，`self.age 等价于 [self age]`，调用`getter`方法，返回`_age`；

## @property

`@property` 关键字可以自动生成某个成员变量的`setter`和`getter`方法的声明

编译时遇到这一行，则自动扩展成下面两句：

- `- (void)setAge:(int)age;`
- `- (int)age;`

```objective-c
@interface ViewController ()
@property (assign, nonatomic) int age;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.age = 5;
    NSLog(@"%d", _age);
}

//- (int)age {
//    return 5;
//}

- (void)setAge:(int)age {
    _age = age;
}
```

也可以重新指定getter方法

```objective-c
@interface ViewController ()
@property (assign, nonatomic, getter=myAge) int age;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.age = 5;
    NSLog(@"%d", self.age);
}

//- (int)age {
//    return 10;
//}

- (int)myAge {
    return 20;
}

//- (void)setAge:(int)age {
//    _age = age;
//}

@end
```

### 属性
- #### copy
  
  复制赋值对象，生成新对象
  
  
  
- #### assign

  一般用来修饰基础数据类型(NSInteger, CGFloat) 和 C数据类型(int ,float, double)等。它的setter方法直接赋值，不进行任何retain操作。

  当它们指向的对象释放以后，weak会被自动设置为nil，而assign不会，所以会导致野指针的出现，可能会导致crash

- #### strong
  strong修饰的属性，对属性进行的是强引用，对象的引用计数retainCount + 1

  指向赋值对象，不生成新对象
  
- #### weak

  weak 表示对对象的弱引用，被weak修饰的对象随时可被系统销毁和回收

  weak比较常用的地方就是delegate属性的设置

  用weak修饰弱引用，**不会**使传入对象的引用计数加1

- #### readwrite/readonly (读写策略、访问权限)

  当我们用readwrite修饰的时候表示该属性可读可改，用readonly修饰的时候表示这个属性只可以读取，不可以修改，一般常用在我们不希望外界改变只希望外界读取这种情况

- #### nonatomic/atomic (安全策略)
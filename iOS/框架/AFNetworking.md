AFNetworking主要分为四个模块：

处理请求和回复的序列化模块：`Serialization`<br>
网络安全模块：`Security`<br>
网络监测模块：`Reachability`<br>

处理通讯的会话模块：`NSURLSession`<br>

其中NSURLSession是最常使用的模块，也是综合模块，它引用了其他的几个模块，而其他几个模块都是独立的模块，所以对AFNetworking的学习就先从这些单独的模块开始。

### Serialization模块
Serialization模块包括请求序列化`AFURLRequestSerialization`和响应序列化`AFURLResponseSerialization`，它们主要功能：

`AFURLRequestSerialization`: 用来将字典参数编码成URL传输参数，并提供上传文件的基本功能实现。

`AFURLResponseSerialization`: 用来处理服务器返回数据，提供返回码校验和数据校验的功能。

#### AFURLRequestSerialization.h
AFURLRequestSerialization协议可以被一个编码特定http请求的对象实现。
请求序列化器（Request serializer）可以编码查询语句、HTTP请求体，如果必须的话，可以自行设置合适的HTTP请求体内容（如：Agent:iOS）。
例如，一个JSON请求序列化器会把请求体Content-Type设置为application/json。

#### AFURLResponseSerialization.h
AFURLResponseSerialization协议是用于将数据根据服务器返回的详细信息解码成更有效的对象表示（object representation）。回复序列化器（Response serializers）还可以为返回的数据提供校验。
例如：一个JSON response serializer 可能在检测状态码（’2xx’范围内）和Content-Type（‘application/json’）后，将JSON response解码一个对象。

### Security模块
虽然苹果目前已经要求应用都开启ATS功能（App Transport Security）强制使用HTTPS来做请求以此加强网络传输的安全，但是HTTPS也可能会遭受中间人攻击（使用Charles抓包时就用到了这种技术），幸运的是在Client-Server架构中，可以通过SSL绑定（pinnig）来进一步加强网络安全传输。SSL绑定即是将证书放到APP内，在进行网络请求时使用APP内的证书进行验证。

### Reachability模块
#### AFNetworkReachabilityManager
AFNetworkReachabilityManager用于监听域名或者IP地址的可达性，包括WWAN和WiFi网络接口。
Reachability可以被用来确定一个网络操作失败的后台信息，或者当连接建立时触发网络操作重试。
它不应该用于阻止用户发起网络请求，因为可能需要第一个请求来建立可达性。
AFNetworkReachabilityManager主要通过系统库的方法来实现网络监听,并以Block的方式进行回调

### NSURLSession模块
NSURLSession是用于处理HTTP请求的类，通过一个NSURLSessionConfiguration对象配置处理Cache、Cookies、Credibility的方式，并通过一系列协议来对请求进行处理


















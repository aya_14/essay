第三方登录
苹果更新的审核规范中提到使用第三方登录的APP必须要将apple登录作为一个可选择项

```xml
<key>UIUserInterfaceStyle</key>
<string>Light</string>
```

这样可以在APP内禁用掉暗黑模式。但长远来说，苹果还是希望我们的APP会支持暗黑模式，不久的将来，会强制APP开发者去适配。


## copy & mutableCopy

#### 对象为不可变数组

```objective-c
NSArray *arr = @[@"A", @"B", @"c"];

// ---
NSMutableArray *copyarr = [[NSMutableArray alloc] init];
copyarr = [arr copy];
NSMutableArray *deeparr = [[NSMutableArray alloc] init];
deeparr = [arr mutableCopy];

or

NSArray *deeparr = [arr mutableCopy];
NSArray *copyarr = [arr copy];
// ---

NSLog(@"arr :%p", arr);
NSLog(@"copyarr :%p", copyarr);
NSLog(@"deeparr :%p", deeparr);
```

输出结果：

```
arr 		:0x10072c310
copyarr :0x10072c310
deeparr :0x10072c880
```

copy 为浅拷贝，mutableCopy为深拷贝

#### 对象为可变数组

```objective-c
NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects:@"A", @"B", @"c", nil];

// ---
NSMutableArray *copyarr = [[NSMutableArray alloc] init];
copyarr = [arr copy];
NSMutableArray *deeparr = [[NSMutableArray alloc] init];
deeparr = [arr mutableCopy];

or

NSArray *deeparr = [arr mutableCopy];
NSArray *copyarr = [arr copy];
// ---

NSLog(@"arr :%p", arr);
NSLog(@"copyarr :%p", copyarr);
NSLog(@"deeparr :%p", deeparr);
```

输出结果：

```
arr 		:0x100748990
copyarr :0x100748000
deeparr :0x100748f20
```

copy 、mutableCopy皆为深拷贝
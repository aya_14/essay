添加对消息的监听，执行对应的方法
> name: 通知的名称

> object: 通知的发布者
```objc
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recvMsg:) name:@"msg" object:@"RecvMsg"];
```

发送消息
> name: 通知的名称

> object: 通知的发布者

> userInfo: 传值
```objc
[[NSNotificationCenter defaultCenter] postNotificationName:@"msg" object:@"RecvMsg" userInfo:@{@"msg":msg}];
```

通知的注销
```objc
[[NSNotificationCenter defaultCenter] removeObserver:self];
```

通知的生成有两个地方
1. viewWillAppear
2. viewDidLoad

注销也有两个地方供选择
1. viewWillDisappear
2. dealloc

这得和业务场景匹配才行，有些情况下接受通知可以伴随着对象的生命周期，建议在init-dealloc这对中注册取消。如果是伴随着UI显示而接收通知，则在didappear和diddisappear中进行最好（and在dealloc补充个取消，因为在navigation poptoroot的时候，中间的一些VC不会出发disappear等函数）。
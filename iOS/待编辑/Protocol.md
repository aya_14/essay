```objective-c
@protocol AnimalProtocol <NSObject>
- (void)playFootball;
- (void)playBasketball;
- (void)run;
@end
```
我们经常使用的QQ空间,当我们短时间内两次进入页面的时候,页面是没有重新创建的,不光是缓存的数据,是整个页面都没有变.我们在开发中可能也会有这样的需求.

开始的时候,想的思路是这样的: 用定时器,将页面挂起,不销毁,再将其pop出来.
```objc
TaskShowViewController * taskVC;

NSArray * controllers = self.navigationController.viewControllers;

for (UIViewController * viewController in controllers) {
    
    if ([viewController.title isEqualToString:@"任务"]) {
        
        taskVC = (TaskShowViewController*)viewController;
    }else{
        taskVC =[[TaskShowViewController alloc]init];
    }

}

[self.navigationController pushViewController:taskVC animated:YES];
```
但是发现只要pop的页面就不会出现在数组中,是拿不到的.

于是通过别的方式实现:
在push页面保存这个将要弹出的页面,然后在弹出的时候启动页定时器,时间自己设定,就是在某一时间段后弹出的页面会销毁.
```objc
  if (saveTaskVC ==nil) {
    
    TaskShowViewController * worksVC =[[TaskShowViewController alloc]init];
    
    saveTaskVC = worksVC;
    
    NSTimer * tiemr =[NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer * _Nonnull timer) {
        saveTaskVC = nil;
        timer = nil;
        [timer invalidate];
    }];
}

[self.navigationController pushViewController:saveTaskVC animated:YES];
```
saveTaskVC 便是保存的控制器,在设定好时间以后便将其销毁,push出的页面也会销毁.
传递指针，数组的本质就是`连续地址+指针`

举个例子：
```objc
- (void)viewDidLoad {
    [super viewDidLoad];
    
    int dataArray[10][8] ={
        {0, 0, 10, 0, 0, 0, 22, 0},
        {0, 5, 10, 30, 0, 30, 21, 0},
        {5, 20, 50, 40, 10, 50, 19, 0},
        {5, 40, 60, 50, 20, 60, 17, 0},
        {12, 60, 80, 60, 40, 80, 14, 0},
        {20, 80, 80, 80, 80, 80, 12, 0},
        {10, 60, 60, 60, 60, 60, 11, 0},
        {10, 40, 40, 40, 30, 40, 10, 0},
        {5, 20, 20, 20, 0, 20, 9, 0},
        {0, 10, 10, 10, 0, 10, 8, 0}
    };
    
    [self test:dataArray];
}
- (void)test:(int *)data{
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 8; j++) {
            NSLog(@"%d", *(data + 8 * i + j));
        }
    }
}
```
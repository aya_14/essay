# 介绍
`CocoaPods`是一个用来帮助我们管理第三方依赖库的工具。它可以解决库与库之间的依赖关系，下载库的源代码，同时通过创建一个`Xcode`的`workspace`来将这些第三方库和我们的工程连接起来，供我们开发使用。

**使用`CocoaPods`的目的是让我们能自动化的、集中的、直观的管理第三方开源库。**

# 安装
1.查看当前的源:

    $ gem sources -l
    $ gem sources --remove https://rubygems.org/
    $ gem sources -a https://gems.ruby-china.org/

2.要输入密码，为电脑开机密码

    sudo gem install cocoapods

### 但是这样安装巨慢~~~
首先

    cd ~/.cocoapods/repos
当然如果没有的话就自己创建一个。

然后去这儿

[https://github.com/CocoaPods/Specs](https://github.com/CocoaPods/Specs)

![](http://upload-images.jianshu.io/upload_images/1389689-9a7433f9adaa6660.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
将`zip`下载下来，解压到`repos`目录，把文件名改为`master`

最后

    git init


# 使用
1. `cd /文件夹的目录`     (cd与文件夹目录之间一定要有空格)
2. `vim Podfile`(生成并进入Podfile文件)
3. 在Podfile文件中:
    ```
    //平台
    platform :ios, ‘8.0'
    target “文件夹名字” do
        pod ‘FMDB’
        //pod "AFNetworking", "~> 2.0" #可以指定目标版本
    end
    ```
4. `pod install`(不更新本地仓储，在后面加上`--verbose --no-repo-update`)
5. `pod update`在修改Podfile之后，使用此命令来更新本地库
6. `pod search XXX`搜索 XXX 库，可以查看最新的版本



https://www.cnblogs.com/hs-funky/p/6759977.html
bounds是以view1左上角为坐标原点的对view1位置、大小的说明，所以bounds的位置一般都是{0，0}（坐标变换遵循：上加下减，左加右减）

frame是以设备左上角为坐标原点的对view1位置、大小的说明
```objective-c
UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(20, 20, 280, 250)];
//[view1 setBounds:CGRectMake(-20, -20, 280, 250)];
//上面代码等同于下面，改变bounds
//CGRect n = view1.bounds;
//n.origin.x = 20;
//n.origin.y = 20;
//view1.bounds = n;
view1.backgroundColor = [UIColor redColor];
[self.view addSubview:view1];//添加到self.view
NSLog(@"view1 frame:%@========view1 bounds:%@",NSStringFromCGRect(view1.frame),NSStringFromCGRect(view1.bounds));
    
UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
view2.backgroundColor = [UIColor yellowColor];
[view1 addSubview:view2];//添加到view1上,[此时view1坐标系左上角起点为(-20,-20)]
NSLog(@"view2 frame:%@========view2 bounds:%@",NSStringFromCGRect(view2.frame),NSStringFromCGRect(view2.bounds));
```

输出：
```
2017-11-07 11:39:30.763014+0800 animation[5681:3673187] view1 frame:{{20, 20}, {280, 250}}========view1 bounds:{{0, 0}, {280, 250}}
2017-11-07 11:39:30.763197+0800 animation[5681:3673187] view2 frame:{{0, 0}, {100, 100}}========view2 bounds:{{0, 0}, {100, 100}}
```

改变bounds后： 输出：
```
2017-11-07 13:46:13.940722+0800 animation[5889:4012742] view1 frame:{{20, 20}, {280, 250}}========view1 bounds:{{20, 20}, {280, 250}}
2017-11-07 13:46:13.940965+0800 animation[5889:4012742] view2 frame:{{0, 0}, {100, 100}}========view2 bounds:{{0, 0}, {100, 100}}
```
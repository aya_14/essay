每一个App有且仅有一个UIWindow

```
-- UIWindow
  -- UIViewController
    -- UIView
```

1. ```[UIApplication sharedApplication].windows```在本应用中打开的UIWindow列表，这样就可以接触应用中的任何一个UIView对象(平时输入文字弹出的键盘，就处在一个新的UIWindow中)。

2. ```[UIApplication sharedApplication].keyWindow```（获取应用程序的主窗口）用来接收键盘以及非触摸类的消息事件的UIWindow，而且程序中每个时刻只能有一个UIWindow是keyWindow。

3. ```view.window```获得某个UIView所在的UIWindow

如何销毁一个UIWindow
```
self.testWindow.hidden = YES;
self.testWindow = nil;
```
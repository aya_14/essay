## UICollectionView

- ##### minimumInteritemSpacing
  ![minimumInteritemSpacing](/UICollectionView/img/minimumInteritemSpacing.png)

- ##### minimumLineSpacing
  ![minimumLineSpacing](/UICollectionView/img/minimumLineSpacing.png)

- ##### sectionInset
  ![sectionInset](/UICollectionView/img/sectionInset.png)
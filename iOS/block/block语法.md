# block介绍
Blocks是C语言的扩充功能。可以用一句话来表示Blocks的扩充功能：带有自动变量（局部变量）的匿名函数。命名就是工作的本质，函数名、变量名、方法名、属性名、类名和框架名都必须具备。而能够编写不带名称的函数对程序员来说相当有吸引力。 例如：我们要进行一个URL的请求。那么请求结果以何种方式通知调用者呢？通常是经过代理（delegate）但是，写delegate本身就是成本，我们需要写类、方法等等。 这时候，我们就用到了block。block提供了类似由C ++和OC类生成实例或对象来保持变量值的方法。像这样使用block可以不声明C + +和OC类，也没有使用静态变量、静态全局变量或全局变量，仅用编写C语言函数的源码量即可使用带有自动变量值的匿名函数。 其他语言中也有block概念。

## block语法 -> 块语法(闭包)
组成：
1. 函数指针
2. 结构体

> @property(copy, nonatomic) void (^block0)(NSString *str);

1. 无返回值，无参数
```objc

void (^block1) (void) = ^(void){
    NSLog(@"block1 无返回值，无参数");
};

```
2. 无返回值，有参数
```objc

void (^block2) (int) = ^(int x){
    NSLog(@"block2 无返回值，有参数:%d",x);
}; 

```
3. 有返回值，无参数
```objc

int (^block3) (void) = ^(void){
    return 1;
};

```
4. 有返回值，有参数
```objc

int (^block4) (int, int) = ^(int x, int y) {
    return x + y;
};

```

> 局部变量需要加修饰符__block，才能在block中修改局部变量的值。

> 全局变量则不需要。

```objc
//定义全局block
typedef void(^block5) (NSString *str);

//避免循环引用
__weak xxxController *weakSelf = self;

__weak typeof(self) weakSelf = self;

```


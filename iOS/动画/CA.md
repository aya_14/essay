CAKeyframeAnimation 的 values 记录的是最初的状态（UIBezierPath表示）、最终的状态（UIBezierPath表示）和一些中间态（UIBezierPath表示）

currentPoint 当前path，最后一个点的坐标

CABasicAnimation 的 fromValue 和 toValue，以一个点作为参照点，这两者之间的差值则作为动画的位移距离。

CABasicAnimation *anima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
用strokeEnd来控制动画的进程。

画图形时，注意：从开头到结尾，按图形的走向去画，这是”一笔画“。
![1](/动画/img/1.png)

`比如说这个图，假设从A开始，一条直线到B，再到C。从C开始，逆时针，M_PI_2*3到M_PI_2，到D。这个画的方向一定得对，否则就乱七八糟了。然后，连线E、F。这时，再画半圆，从G开始，逆时针，M_PI_2到M_PI_2*3，到H，然后封闭曲线。`
```objective-c
- (void)drawRect:(CGRect)rect {
    CGFloat r = 5;
    UIBezierPath *left = [UIBezierPath bezierPathWithArcCenter:CGPointMake(0, self.frame.size.height/3.0) radius:r startAngle:-M_PI_2 endAngle:M_PI_2 clockwise:YES];
    [left addLineToPoint:CGPointMake(0, self.frame.size.height)];
    [left addLineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height)];
    [left addArcWithCenter:CGPointMake(self.frame.size.width, self.frame.size.height/3.0) radius:r startAngle:M_PI_2 endAngle:M_PI_2*3 clockwise:YES];
    [left addLineToPoint:CGPointMake(self.frame.size.width, 0)];
    [left addLineToPoint:CGPointMake(0, 0)];
    
    CAShapeLayer *leftShape = [CAShapeLayer layer];
    leftShape.frame = self.bounds;
    leftShape.path = left.CGPath;
    self.layer.mask = leftShape;
}
```
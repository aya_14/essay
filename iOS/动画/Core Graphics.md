# Core Graphics/QuartZ 2D

> QuartZ 2D是苹果公司开发的一套API，它是Core Graphics Framework 的一部分。

> Core Graphics API 所有的操作都在一个上下文中进行。所以再绘图之前需要获取该上下文并传入执行渲染的函数中。如果你正在渲染一副在内存中的图片，此时就需要传入图片所属的上下文。获得一个图形上下文是我们完成绘图任务的第一步，你可以将图形上下文理解为一块画布。如果你没有得到这块画布，那么你就无法完成任何绘图操作。

### 画线
首先，要了解线是如何绘制的。确定起点和终点来确定一条线。
```objc
- (void)drawRect:(CGRect)rect {
    //1.获取上下文
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    //2.描述路径
    UIBezierPath * path = [UIBezierPath bezierPath];
    //起点
    [path moveToPoint:CGPointMake(10, 10)];
    //终点
    [path addLineToPoint:CGPointMake(100, 100)];
    //设置颜色
    [[UIColor whiteColor]setStroke];
    //设置线宽
    CGContextSetLineWidth(contextRef, 2);
    //3.添加路径
    CGContextAddPath(contextRef, path.CGPath);
    //显示路径
    CGContextStrokePath(contextRef);

}
```

### 画多边形
首先，要了解多边形是如何绘制的。多边形是有多条线段组成的，确定每条线段拐角处的坐标来确定一个多边形。
```objc
- (void)drawRect:(CGRect)rect {
    //获取上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //描述路径
    UIBezierPath *path = [UIBezierPath bezierPath];
    //画路径
    [path moveToPoint:CGPointMake(10, 10)];
    [path addLineToPoint:CGPointMake(110, 10)];
    [path addLineToPoint:CGPointMake(110, 110)];
    [path addLineToPoint:CGPointMake(10, 110)];
    //闭合路径,相当于[path addLineToPoint:CGPointMake(10, 10)];
    [path closePath];
    //上色
    [[UIColor blackColor] setStroke];
    //添加路径
    CGContextAddPath(ctx, path.CGPath);
    //显示路径
    CGContextStrokePath(ctx);
}
```
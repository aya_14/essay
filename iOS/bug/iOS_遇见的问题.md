#### 1. Block implicitly retains 'self';
##### 原因：
> 升级到Xcode9.3之后，在大括号里声明的属性，出现在block内，就会发出此警告⚠️

##### 解决：
> Building Settings -> 搜索 implicit retain of 'self' whitin blocks -> No

---

#### 2. Lexical or preprocessor issue;
##### 原因:
> pch文件路径不对，或许被设置成了绝对路径❌

##### 解决:
> Building Settings -> 搜索 Prefix Header -> $(PROJECT_DIR)/文件的路径

---

#### 3. Multiple plist;
##### 原因:
> xcode 9 -> xcode 10 后出现这个问题❌

##### 解决:
> File -> Workspace/Project settings -> Bulid System -> Legacy Bulid System
1. xib布局按钮，**未设背景色**，然后给按钮的`layer`层设置属性：CAGradientLayer
```
出现问题：整个按钮有一大半实现了效果，但有一部分没颜色。这个bug只出现在了plus设备上。

解决：给按钮添加一个背景色就行。
```
2. `iOS 11` 后`ScrollView`偏移
```
出现问题：在iOS 11上运行Scrollview向下偏移64px或者20px，因为iOS 11废弃了automaticallyAdjustsScrollViewInsets，给UIScrollView增加了contentInsetAdjustmentBehavior属性。解决这个问题我们需要做一下系统判断就可以，Tableview与Collectionview类似的解决方案

解决：
if (@available(iOS 11.0, *)) {
    _scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
}else {
    self.automaticallyAdjustsScrollViewInsets = NO;
}
```
以下几种情况`layoutSubviews`会被调用。
- 直接调用 setNeedsLayout
- addSubview（frame != CGRectZero） 的时候
- 当 view 的 frame(Height/Width))/bounds(All) 发生改变的时候
- 滑动 UIScrollView 的时候，会触发 scrollview 的 layoutSubviews
- 旋转 Screen 会触发 父UIView 上的 layoutSubviews 事件
- 当 view1 addSubview view2（view1,2并不是继承关系）时，改变 view2 大小 frame(Height/Width))/bounds(All) 的时候也会触发 view1 上的 layoutSubviews 事件；若只改变view1时，view2 的 layoutSubviews 是不会触发的

#### setNeedsLayout
> 标记为需要重新布局，异步调用layoutIfNeeded刷新布局，不立即刷新，在下一轮runloop结束前刷新，对于这一轮runloop之内的所有布局和UI上的更新只会刷新一次，layoutSubviews一定会被调用。
> 一定会调用，但在下一个周期触发```- (void)layoutSubviews```

#### layoutIfNeeded
> 如果有需要刷新的标记，立即调用layoutSubviews进行布局（如果没有标记，不会调用layoutSubviews）。
> 不一定会调用，但调用一定触发```- (void)layoutSubviews```

在当前的Runloop中立即刷新布局:
```objective-c
[self setNeedsLayout];
[self layoutIfNeeded];
```
---
iOS 13（单窗口）后获取状态栏高度
```objective-c
UIStatusBarManager *statusBarManager = [[UIApplication sharedApplication].windows firstObject].windowScene.statusBarManager;
    NSLog(@"%f", statusBarManager.statusBarFrame.size.height);
```
对于创建的view 
y坐标 < 状态栏高度，`- (void)layoutSubviews`会调用 2 次；
y坐标 >= 状态栏高度，则只会调用 1 次。
当此时控制器添加上导航栏后，状态栏高度+导航栏高度44px，同样会导致`- (void)layoutSubviews`调用次数不同，情形同上。

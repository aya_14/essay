```objective-c
    @property (strong, nonatomic) UISearchBar *searchBar;

    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,375-100,30)];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 375 - 100, 30)];
    self.searchBar.placeholder = @"搜索";
    self.searchBar.layer.cornerRadius = 15;
    self.searchBar.layer.masksToBounds = YES;
    //设置背景色
    self.searchBar.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.1];
    // 修改cancel
    self.searchBar.showsCancelButton = NO;
    // 修改cancel
    self.searchBar.showsSearchResultsButton = NO;
    //5. 设置搜索Icon
    [self.searchBar setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    //一下代码为修改placeholder字体的颜色和大小
    UITextField* searchField = [_searchBar valueForKey:@"_searchField"];
    //2. 设置圆角和边框颜色
    // kvc
    if(searchField) {
        [searchField setBackgroundColor:[UIColor clearColor]];
        // 根据@"_placeholderLabel.textColor" 找到placeholder的字体颜色
        [searchField setValue:[UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    }
    // 输入文本颜色
    searchField.textColor = [UIColor whiteColor];
    searchField.font = [UIFont systemFontOfSize:15];
    // 默认文本大小
    [searchField setValue:[UIFont boldSystemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    //只有编辑时出现出现那个叉叉
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [titleView addSubview:self.searchBar];
    //Set to titleView
    self.navigationItem.titleView = titleView;
```
### UINavigationBar图层
```
1. UINavigationBar
2.     _UIBarBackground
3.         UIImageView(线)
4.             UIVisualEffectView
5.                 _UIVisualEffectBackdropView
6.                     _UIVisualEffectFilterView
```

### 导航栏左右的按钮
#### 1. title
> 自定义文字
```objc
UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"hahha" style:UIBarButtonItemStylePlain target:self action:@selector(doSomething:)];
self.navigationItem.leftBarButtonItem = leftItem;
```
其中style有2种样式
```objc
typedef NS_ENUM(NSInteger, UIBarButtonItemStyle) {
    UIBarButtonItemStylePlain,
    UIBarButtonItemStyleBordered NS_ENUM_DEPRECATED_IOS(2_0, 8_0, "Use UIBarButtonItemStylePlain when minimum deployment target is iOS7 or later"),
    UIBarButtonItemStyleDone,
};
```
- UIBarButtonItemStylePlain
![image](http://img1.ph.126.net/IscZyDj5FvRv_uY6EGzM6g==/6632753317024323568.png)
- UIBarButtonItemStyleDone
![image](http://img0.ph.126.net/QGlj0Pw1DEkRWXA7R2SkFg==/6632456448887414391.png)
#### 2. image
> 自定义图片
```objc
UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"xxx"] style:UIBarButtonItemStylePlain target:self action:@selector(doSomething:)];
self.navigationItem.leftBarButtonItem = leftItem;
```
#### 3. systemItem
> 系统图标
```objc
UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(doSomething:)];
self.navigationItem.leftBarButtonItem = leftItem;
```
以下为系统的```UIBarButtonSystemItem```
```objc
typedef NS_ENUM(NSInteger, UIBarButtonSystemItem) {
    UIBarButtonSystemItemDone,
    UIBarButtonSystemItemCancel,
    UIBarButtonSystemItemEdit,  
    UIBarButtonSystemItemSave,  
    UIBarButtonSystemItemAdd,
    UIBarButtonSystemItemFlexibleSpace,
    UIBarButtonSystemItemFixedSpace,
    UIBarButtonSystemItemCompose,
    UIBarButtonSystemItemReply,
    UIBarButtonSystemItemAction,
    UIBarButtonSystemItemOrganize,
    UIBarButtonSystemItemBookmarks,
    UIBarButtonSystemItemSearch,
    UIBarButtonSystemItemRefresh,
    UIBarButtonSystemItemStop,
    UIBarButtonSystemItemCamera,
    UIBarButtonSystemItemTrash,
    UIBarButtonSystemItemPlay,
    UIBarButtonSystemItemPause,
    UIBarButtonSystemItemRewind,
    UIBarButtonSystemItemFastForward,
    UIBarButtonSystemItemUndo NS_ENUM_AVAILABLE_IOS(3_0),
    UIBarButtonSystemItemRedo NS_ENUM_AVAILABLE_IOS(3_0),
    UIBarButtonSystemItemPageCurl NS_ENUM_AVAILABLE_IOS(4_0),
};
```
### tabBar 创建流程
1. 新建UITabBarController，为tabBarVC
2. 在AppDelegate中，设置rootViewController为tabBarVC
3. 在tabBarVC中，引入需要包含的viewController，设置他们的title、image等等


- tabBar.tintColor        设置tabBar显示颜色

- tabBar.barTintColor     设置tabBar的背景颜色

- tabBar.backgroundImage  为tabBar添加背景图片

背景图 | 高度49 | 宽度看设备
:---:|:---:|:---:
大于 | 底部tabBar的高度根据图片高度来 | 从左到右截取
小于 | 不足的部分会再次用背景图补齐 | 不足的部分会再次用背景图补齐

#### UITabBar图层
```
1. UITabBar
2.    _UIBarBackground
3.        UIImageView(线)
4.            UIVisualEffectView
5.                _UIVisualEffectBackdropView
6.                    _UIVisualEffectSubview[old:_UIVisualEffectFilterView(毛玻璃)]
7.                        UITabBarButton
8.                            UITabBarSwappableImageView
9.                            UITabBarButtonLabel
```
- 设置backgroundColor时，[1. 5.]颜色会改变，但由于[6.]的效果，呈现一种朦胧感。
- 设置barTintColor时，[6.]变成成设置的颜色，[1. 5.]不变。
- 设置tintColor时，[8. 9.]会被渲染成设置的颜色。

#### 设置背景图时UITabBar图层
```
1. UITabBar
2.     _UIBarBackground         
3.         UIImageView(背景图)
4.             UIImageView(线)
5.             UITabBarButton
6.                 UITabBarSwappableImageView
7.                 UITabBarButtonLabel
```

### 动态tabbar
```objc
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSInteger index = [self.tabBar.items indexOfObject:item];
    if (index != _indexflag) {
        NSMutableArray *arr = [NSMutableArray array];
        for (UIView *btnView in self.tabBar.subviews) {
            if ([btnView isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
                for (UIView *imageView in btnView.subviews) {
                    if ([imageView isKindOfClass:NSClassFromString(@"UITabBarSwappableImageView")]) {
                        [arr addObject:imageView];
                    }
                }
            }
        }
        
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    //速度控制函数，控制动画运行的节奏
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.duration = 0.2;       //执行时间
    animation.repeatCount = 1;      //执行次数
    animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
    animation.fromValue = [NSNumber numberWithFloat:0.9];   //初始伸缩倍数
    animation.toValue = [NSNumber numberWithFloat:1.0];     //结束伸缩倍数
    [[arr[index] layer] addAnimation:animation forKey:nil];
    //记录上一个tabBar index
    _indexflag = index;
    }
}
```

![image](http://img1.ph.126.net/SrqC91Rob0lz_2aEBkjOig==/1273111319680722076.png)
上面的4个```UITabBarButton```为创建的，遍历```tabBar.subviews```，取出```UITabBarButton```。如果为这个添加动画，那么```tabBar```上的标题和图标都会动，不太好看。

![image](http://img1.ph.126.net/HoGSFWDkTeA2CySnJCtflw==/93168217309639486.png)
```po```一下```UITabBarButton.subviews```，会看到```UITabBarButton```上的两个控件
```
1. UITabBarSwappableImageView
2. UITabBarButtonLabel
```
这时，可以单独为图标或者标题做动画。

```
-(void)btnClickMethod{

    ThirdViewController *thirdVc = [[ThirdViewController alloc]init];
    //跳转隐藏tabBar
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:thirdVc animated:YES];
    //返回时候显示tabBar
    self.hidesBottomBarWhenPushed = NO;
}
```
包含了`Photos`下所有头文件

```objective-c
#import <Photos/PHPhotoLibrary.h>
// Photos 枚举类型集合
#import <Photos/PhotosTypes.h>
#import <Photos/PHError.h>

#import <Photos/PHObject.h>
// 代表系统的一个图片、视频或者Live Photo
#import <Photos/PHAsset.h>
#import <Photos/PHLivePhoto.h>
// 是一组有序的资源集合,包括相册、moments、智能相册以及共享照片流.比如：系统相册里的时刻一个分类，用户创建的相册或者智能相册
#import <Photos/PHCollection.h>

// option的集合，对asset对象进行 过滤，排序和管理
#import <Photos/PHFetchOptions.h>
#import <Photos/PHFetchResult.h>

#import <Photos/PHChange.h>

#import <Photos/PHAssetChangeRequest.h>
#import <Photos/PHAssetCreationRequest.h>
#import <Photos/PHAssetCollectionChangeRequest.h>
#import <Photos/PHCollectionListChangeRequest.h>
#import <Photos/PHLivePhotoEditingContext.h>

#import <Photos/PHImageManager.h>

#import <Photos/PHAssetResourceManager.h>
#import <Photos/PHAssetResource.h>

#import <Photos/PHAdjustmentData.h>
#import <Photos/PHContentEditingInput.h>
#import <Photos/PHContentEditingOutput.h>

// 可以直接使用 localIdentifier 属性对PHObject及其子类对象进行对比是否同一个对象
#import <Photos/PHProject.h>
#import <Photos/PHProjectChangeRequest.h>
#import <Photos/PHCloudIdentifier.h>
```

注：
`BurstAssets`：连拍资源

## CoreFoundation
`Core Foundation`框架(CoreFoundation.framework)是一组C语言接口，它们为iOS应用程序提供基本数据管理和服务功能。

访问的低级函数，原始数据类型和各种集合类型，并与Foundation框架无缝桥接。

`Core Foundation`是一个框架，可为应用程序服务，应用程序环境以及应用程序本身提供基础软件服务。 `Core Foundation`还提供常见数据类型的抽象，便于使用Unicode字符串存储进行国际化，并提供一系列实用程序，如插件支持，XML属性列表，URL资源访问和偏好设置。

- 群体数据类型 (数组、集合等)
- 程序包
- 字符串管理
- 日期和时间管理
- 原始数据块管理
- 偏好管理
- URL及数据流操作
- 线程和RunLoop
- 端口和soket通讯
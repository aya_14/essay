1. iOS11中 UIViewController 的 automaticallyAdjustsScrollViewInsets 属性被废弃了，因此当tableView超出安全区域时，系统自动会调整SafeAreaInsets值，进而影响adjustedContentInset值
```objc
// 有些界面以下使用代理方法来设置，发现并没有生效
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
// 这样的原理是因为之前只是实现了高度的代理方法，却没有实现View的代理方法，iOS10及以前这么写是没问题的，iOS11开启了行高估算机制引起的bug，因此有以下几种解决方法：

// 解决方法一：添加实现View的代理方法，只有实现下面两个方法，方法 (CGFloat)tableView: heightForFooterInSection: 才会生效
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

// 解决方法二：直接使用tableView属性进行设置,修复该UI错乱
self.tableView.sectionHeaderHeight = 0;
self.tableView.sectionFooterHeight = 5;
[_optionTableView setContentInset:UIEdgeInsetsMake(-35, 0, 0, 0)];

// 解决方法三：添加以下代码关闭估算行高
self.tableView.estimatedRowHeight = 0;
self.tableView.estimatedSectionHeaderHeight = 0;
self.tableView.estimatedSectionFooterHeight = 0;
```

2. 导航栏 20 px -> 44px
3. Tabbar 49px -> 83px
```
#define kStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavBarHeight 45.0
#define kTabBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)
#define kTopHeight (kStatusBarHeight + kNavBarHeight)
```

4. 相册权限变更
```
iOS11之后：默认开启访问相册权限（读权限），无需用户授权，无需添加NSPhotoLibraryUsageDescription，适配iOS11之前的还是需要加的。 添加图片到相册（写权限），需要用户授权，需要添加 NSPhotoLibraryAddUsageDescription。

总结：iOS11之后如果需要写入权限需要添加NSPhotoLibraryAddUsageDescription字段。

NSPhotoLibraryUsageDescription：
Property List：
Privacy - Photo Library Usage Description
Source Code：
NSPhotoLibraryUsageDescription 是否允许此APP使用相册？

NSPhotoLibraryAddUsageDescription ：
Property List：
Privacy - Photo Library Additions Usage Description
Source Code:
NSPhotoLibraryAddUsageDescription 是否允许此APP保存图片到相册？
```
#!/bin/bash
# 文件路径不要有中文
# 执行：bash iOS_project_replace.sh路径 项目路径 更改后的项目名 更改后的作者名
# 项目路径       $1
# 更改后的项目名  $2
# 更改后的作者名  $3
# 注：略过参数则以 ''(单引号) 替代

# 遍历文件夹下的所有文件
Getdir() {
    for element in `ls $1`
    do
        dir_or_file=$1"/"$element
        if [ -d $dir_or_file ]
        then
            Getdir $dir_or_file
        else
            echo $dir_or_file     
        fi
    done
}

# 忽略特点类型文件/文件夹
# .xcodeproj
# Assets.xcassets
# Podfile.lock
# Podfile
# Pods

# IgnoreFolder(){

# }

Getdir $1

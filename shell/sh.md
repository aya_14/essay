`Linux`的`Shell`种类众多，常见的有：
- Bourne Shell（/usr/bin/sh或/bin/sh）
- Bourne Again Shell（/bin/bash）
- C Shell（/usr/bin/csh）
- K Shell（/usr/bin/ksh）
- Shell for Root（/sbin/sh）
- zsh( https://www.cnblogs.com/dhcn/p/11666845.html )

查看系统安装的shell
> `cat /etc/shells`

在一般情况下，人们并不区分`Bourne Shell`和`Bourne Again Shell`，所以，像`#!/bin/sh`，它同样也可以改为`#!/bin/bash`。
```? 何为一般情况```
> `#!` 告诉系统其后路径所指定的程序即是解释此脚本文件的 Shell 程序。

